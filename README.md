# Racket DNS resolver and server

This is a [Racket](http://racket-lang.org/) implementation of a DNS
server and iterative resolver. It's written to work with
[Syndicate](https://github.com/tonyg/syndicate), but could readily
be adapted to work with other I/O substrates. (It originally used
Racket's `sync` and events directly.)

The code is not quite fully separated into a reusable library (or even
a configurable service) yet. Changing this is straightforward, but
low-priority right now. Patches welcome!

## How to compile and run the code

You will need the latest version of Racket. Any version newer than or
equal to Racket 6.0 should work. Racket releases can be downloaded
[here](http://download.racket-lang.org/).

Once you have Racket installed,

    raco pkg install syndicate bitsyntax

to install Syndicate and
[bitsyntax](https://github.com/tonyg/racket-bitsyntax/), and then

    raco make driver.rkt proxy.rkt

to compile the DNS code. Once it has compiled successfully,

    DNSPORT=5444 racket driver.rkt

will start a DNS leaf zone server on port 5444 holding records from
`test-rrs.rkt`'s `test-rrs` variable, and

    DNSPORT=5444 racket proxy.rkt

will start an iterative DNS resolver service on port 5444 offering
regular DNS service to all comers.

You can try out the service with

    dig @localhost -p 5444 localhost.example

To enable debug output, set the `MATRIX_LOG` environment variable to
`info`:

    MATRIX_LOG=info DNSPORT=5444 racket driver.rkt
    MATRIX_LOG=info DNSPORT=5444 racket proxy.rkt

## Copyright and License

Copyright 2010, 2011, 2012, 2013 Tony Garnock-Jones <tonyg@ccs.neu.edu>

This file is part of marketplace-dns.

marketplace-dns is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

marketplace-dns is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with marketplace-dns. If not, see
<http://www.gnu.org/licenses/>.
